This program can be used for decreasing size of electronic abstracts which 
written in format .tex.

If you want have any positive result of using this program, your .tex file must 
have detailed proof in namespace "proof"